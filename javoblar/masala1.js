//1-masala 1-usul
function myFunction(str) {
    let counter = 0;
    for (let i = 0; i < str.length; i++) {
        if (str[i] === 'a' || str[i] === 'i' || str[i] === 'o' || str[i] === 'e' || str[i] === 'u') {
            counter++;
        }
    }
    return counter
}
console.log(myFunction("salomaaaooi"));



//1-masala 2-usul regex yordamida
function getVowels(str) {
    let m = str.match(/[aeiou]/gi);
    if (m === null) {
        return 0
    } else {
        return m.length
    }
}
console.log(getVowels('salomaaaooi'))